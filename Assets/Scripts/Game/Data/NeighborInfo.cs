﻿using System;
using Game.Components;
using UnityEngine;

namespace Game.Data
{
	public class NeighborInfo
	{
		public NodeComponent Left { get; set; }
		public NodeComponent Right { get; set; }
		public NodeComponent Top { get; set; }
		public NodeComponent Bottom { get; set; }

		public void ForEach(Action<NodeComponent> func)
		{
			NodeComponent[] nodes = {
				Left,
				Right,
				Top,
				Bottom
			};

			int length = nodes.Length;
			for (int i = 0; i < length; i++)
			{
				var current = nodes[i];
				if (current == null) continue;

				func?.Invoke(current);
			}
		}
	}
}