﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Components
{
	public class MousePositionComponent : MonoBehaviour
	{
		[System.Serializable] private class Vector2Event : UnityEvent<Vector2> { }
		[Header("Eventos")]
		[SerializeField] private Vector2Event _mousePosition = null;

		public void EmitMousePosition()
		{
			_mousePosition?.Invoke(Input.mousePosition);
		}
	}
}