﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Handlers
{
	public class CameraRayComponent : MonoBehaviour
	{
		[System.Serializable] private class RaycastHitEvent : UnityEvent<RaycastHit> { }
		[Header("Dependencias")]
		[SerializeField] private Camera _rayCamera = null;
		[Header("Settings")]
		[SerializeField] private LayerMask _hitLayer = Physics.AllLayers;
		[SerializeField] private float _rayDistance = Mathf.Infinity;
		[SerializeField] private QueryTriggerInteraction _triggerInteraction = QueryTriggerInteraction.UseGlobal;

		[Header("Events")]
		[SerializeField] private RaycastHitEvent _hit = null;

		private RaycastHit _raycastHit;

		public void CastRay(Vector2 mousePosition)
		{
			Ray cameraRay = _rayCamera.ScreenPointToRay(mousePosition);
			bool hasHit = Physics.Raycast(cameraRay, out _raycastHit, _rayDistance, _hitLayer, _triggerInteraction);

			Debug.DrawRay(cameraRay.origin, cameraRay.direction * 50f, Color.red, 10f);

			if (hasHit)
			{
				_hit?.Invoke(_raycastHit);
			}
		}
	}
}