﻿using UnityEngine;
using UnityEngine.Events;
namespace Game.Components
{
	public class NodeComponent : MonoBehaviour
	{
		[System.Serializable] private class NodeEvent : UnityEvent<NodeComponent> { }
		[Header("Settings")]
		[SerializeField] private bool _initialState = false;
		[Space]
		[SerializeField] private bool _state = false;

		[Header("Events")]
		[SerializeField] private NodeEvent _stateOn = null;
		[SerializeField] private NodeEvent _stateOff = null;

		public bool InitialState { get => _initialState; set => _initialState = value; }
		public bool State
		{
			get => _state;
			set
			{
				_state = value;
				FireStateEvent();
			}
		}

		public void ToggleState()
		{
			State = !_state;
		}

		public void Initialize()
		{
			State = InitialState;
		}

		public void FireStateEvent()
		{
			// Defino el evento de ejecución según el estado actual del objeto
			var componentEvent = _state ? _stateOn : _stateOff;
			componentEvent?.Invoke(this);
		}
	}
}
