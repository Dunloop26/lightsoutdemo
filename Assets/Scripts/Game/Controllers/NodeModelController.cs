﻿using UnityEngine;

namespace Game.Controllers
{
	public class NodeModelController : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private MeshRenderer _renderer = null;
		[Header("References")]
		[SerializeField] private Material _onMaterial = null;
		[SerializeField] private Material _offMaterial = null;

		public void SetOn()
		{
			_renderer.material = _onMaterial;
		}

		public void SetOff()
		{
			_renderer.material = _offMaterial;
		}

	}
}