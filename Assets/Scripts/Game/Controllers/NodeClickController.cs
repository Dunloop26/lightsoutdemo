﻿using System.Collections;
using System.Collections.Generic;
using Game.Components;
using Game.Manager;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Controllers
{
	public class NodeClickController : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private BoardManager _boardManager = null;
		[Header("Events")]
		[SerializeField] private UnityEvent _clickStart = null;
		[SerializeField] private UnityEvent _clickEnd = null;
		public void ProcessRaycastHit(RaycastHit hit)
		{
			_clickStart?.Invoke();

			// Obtengo el componente nodo a partir de la información del hit
			var node = hit.transform.parent.GetComponent<NodeComponent>();
			if (node == null) return;

			node.ToggleState();
			var neighbor = _boardManager.GetNeighbor(node);

			neighbor.ForEach(element =>
			{
				element.ToggleState();
			});

			_clickEnd?.Invoke();
		}
	}
}