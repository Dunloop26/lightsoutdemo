﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Controllers
{
	public class MouseClickController : MonoBehaviour
	{
		[Header("Events")]
		[SerializeField] private UnityEvent _leftClick = null;
		[SerializeField] private UnityEvent _rightClick = null;
		[SerializeField] private UnityEvent _middleClick = null;

		public void Update()
		{
			// Click izquierdo
			if (CheckMouseButtonClick(0))
			{
				_leftClick?.Invoke();
			}

			// Click derecho
			if (CheckMouseButtonClick(1))
			{
				_rightClick?.Invoke();
			}

			// Click central
			if (CheckMouseButtonClick(2))
			{
				_middleClick?.Invoke();
			}
		}

		public bool CheckMouseButtonClick(int button)
		{
			return Input.GetMouseButtonDown(button);
		}
	}
}