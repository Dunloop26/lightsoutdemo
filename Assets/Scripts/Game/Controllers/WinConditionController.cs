﻿using System;
using System.Collections;
using System.Collections.Generic;
using Game.Manager;
using UnityEngine;
using UnityEngine.Events;

namespace Game.Controllers
{
	public class WinConditionController : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private BoardManager _boardManager = null;
		[Header("Events")]
		[SerializeField] private UnityEvent _win = null;

		public void CheckWinCondition()
		{
			if (HasWon())
			{
				_win?.Invoke();
			}
		}

		private bool HasWon()
		{
			var nodes = _boardManager.GetNodes();

			bool won = true;

			int length = nodes.Length;
			for (int i = 0; i < length; i++)
			{
				if (!nodes[i].State)
				{
					won = false;
					break;
				}
			}

			return won;
		}
	}
}