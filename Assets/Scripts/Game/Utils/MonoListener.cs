﻿using UnityEngine;
using UnityEngine.Events;

namespace Game.Utils
{
	public class MonoListener : MonoBehaviour
	{
		[SerializeField] private UnityEvent _awake = null;
		[SerializeField] private UnityEvent _start = null;
		[SerializeField] private UnityEvent _onEnable = null;
		[SerializeField] private UnityEvent _onDisable = null;
		[SerializeField] private UnityEvent _onDestroy = null;

		private void Awake() => _awake?.Invoke();

		private void Start() => _start?.Invoke();

		private void OnEnable() => _onEnable?.Invoke();

		private void OnDisable() => _onDisable?.Invoke();

		private void OnDestroy() => _onDestroy?.Invoke();
	}
}