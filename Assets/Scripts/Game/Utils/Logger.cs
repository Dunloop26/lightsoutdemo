﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Utils
{
	public class Logger : MonoBehaviour
	{
		public void Log(string message)
		{
			Debug.Log(message);
		}
	}
}