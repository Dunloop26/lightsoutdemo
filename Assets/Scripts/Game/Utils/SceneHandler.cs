﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.Utils
{
	public class SceneHandler : MonoBehaviour
	{
		public void LoadScene(string sceneName)
		{
			SceneManager.LoadScene(sceneName);
		}

		public void LoadSceneAdditive(string sceneName)
		{
			SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
		}

		public void UnloadScene(string sceneName)
		{
			SceneManager.UnloadSceneAsync(sceneName);
		}
	}
}