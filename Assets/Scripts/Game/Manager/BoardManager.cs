﻿using System;
using System.Collections.Generic;
using Game.Components;
using Game.Data;
using UnityEngine;

namespace Game.Manager
{
	public class BoardManager : MonoBehaviour
	{
		[Header("Dependencies")]
		[SerializeField] private NodeComponent _nodePrefab = null;

		[Header("Settings")]
		[SerializeField] private int _size = 8;
		[Space]
		[SerializeField] private Vector2 _prefabSize = new Vector2(1, 1);
		[SerializeField] private float _span = 0f;

		private List<NodeComponent> _gameNodes = new List<NodeComponent>();

		private Dictionary<string, NeighborInfo> _neighborData = new Dictionary<string, NeighborInfo>();

		public void Generate()
		{
			ClearGameNodes();
			Vector3 position = transform.position;

			int _rows = _size;
			int _columns = _size;

			float halfX = (_columns * _prefabSize.x + _span * _columns - 1) / 2f;
			float halfY = (_rows * _prefabSize.y + _span * _rows - 1) / 2f;

			for (int y = 0; y < _rows; y++)
			{
				for (int x = 0; x < _columns; x++)
				{

					position.x = transform.position.x + (x * _prefabSize.x + _span * x) - halfX;
					position.z = transform.position.z + (y * _prefabSize.y + _span * y) - halfY;

					NodeComponent node = Instantiate(_nodePrefab, transform);
					node.name = $"node{x + y * _columns}";
					node.Initialize();
					node.transform.position = position;

					_gameNodes.Add(node);
				}
			}

			ClearNeighborData();
			RegisterNeighbors(_gameNodes.ToArray());
		}
		public NodeComponent[] GetNodes()
		{
			return _gameNodes.ToArray();
		}
		private void ClearNeighborData()
		{
			_neighborData.Clear();
		}

		private void RegisterNeighbors(NodeComponent[] nodeComponents)
		{
			int length = nodeComponents.Length;

			int _rows = _size;
			int _columns = _size;

			for (int y = 0; y < _rows; y++)
			{
				for (int x = 0; x < _columns; x++)
				{
					int idx = ParsePosition(x, y, _columns);
					// Defino los vecinos de la posición actual
					NeighborInfo neighborInfo = new NeighborInfo();

					neighborInfo.Left = SelectNeighbor(nodeComponents, x - 1, y, _columns);
					neighborInfo.Right = SelectNeighbor(nodeComponents, x + 1, y, _columns);
					neighborInfo.Top = SelectNeighbor(nodeComponents, x, y - 1, _rows);
					neighborInfo.Bottom = SelectNeighbor(nodeComponents, x, y + 1, _rows);

					// Registro el nodo y sus vecinos
					var currentNode = nodeComponents[idx];

					if (_neighborData.ContainsKey(currentNode.name))
					{
						Debug.Assert(true);
					}
					_neighborData.Add(currentNode.name, neighborInfo);
				}
			}
		}

		private NodeComponent SelectNeighbor(NodeComponent[] nodeComponents, int x, int y, int limit)
		{
			if (!IsValid(x, limit)) return null;

			int _columns = _size;

			int index = ParsePosition(x, y, _columns);

			if (IsValid(index, nodeComponents.Length))
			{
				return nodeComponents[index];
			}
			else
			{
				return null;
			}
		}

		private bool IsValid(int position, int limit)
		{
			return position >= 0 && position < limit;
		}

		private int ParsePosition(int x, int y, int sizeX)
		{
			return x + y * sizeX;
		}

		public NeighborInfo GetNeighbor(NodeComponent node)
		{
			NeighborInfo info;
			if (_neighborData.TryGetValue(node.name, out info))
			{
				return info;
			}
			else
			{
				return null;
			}
		}

		public void ClearGameNodes()
		{
			_gameNodes.ForEach(
				elemento =>
				{
					Destroy(elemento.gameObject);
				}
			);

			_gameNodes.Clear();
		}
	}
}